#!/usr/bin/python

import argparse
import json
import yaml

def convert_to_webmon(input_steps):
    obj={'site_name':'hello_world'}
    steps=[]
    for input_step in input_steps:
        step={}
        step['url'] = input_step['request']['url']
        step['method'] = input_step['request']['method']
        headers=[]
        for header in input_step['request']['headers']:
            header_str="%s: %s" % (header['name'], header['value'])
            header_={}
            header_['value'] = header_str
            headers.append(header_)
        step['headers'] = headers

        if 'postData' in input_step['request'].keys():
            step['request_body'] = input_step['request']['postData']['text']

        steps.append(step)
    obj['steps'] = steps
    return obj

def convert(input_file, output_file):
    print("Start convert job %s => %s " % (input_file, output_file))

    with open(input_file) as finput:
        har_data = json.load(finput)
        for entries in har_data['log']['entries']:
            print(entries['request']['url'])

    output = convert_to_webmon(har_data['log']['entries'])

    with open(output_file, 'w') as foutput:
        documents = yaml.safe_dump(output, foutput, encoding='utf-8', allow_unicode=True)

def main():
    parser = argparse.ArgumentParser(description='Convert HAR file (HTTP Archive Format) into webmon scenario.')
    parser.add_argument('input', type=str,
                        help='input HAR file')
    parser.add_argument('output', type=str,
                        help='output yml file')

    args = parser.parse_args()

    convert(args.input, args.output)

if __name__ == "__main__":
    # execute only if run as a script
    main()
