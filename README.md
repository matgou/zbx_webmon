# zbx_webmon

Webmonitoring loadable plugin for zabbix to run complex monitoring scenario

# Build

## Prerequis
- libcurl-dev
- libyaml
- lua5.2


## Build & Install

Build can be perform using the following command:
```
autoreconf -fvi
./configure
make clean
make
```
