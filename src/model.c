/******************************************************************************
*   Copyright (C) 2021 Mathieu Goulin <mathieu.goulin@gadz.org>               *
*                                                                             *
*   This program is free software; you can redistribute it and/or             *
*   modify it under the terms of the GNU Lesser General Public                *
*   License as published by the Free Software Foundation; either              *
*   version 2.1 of the License, or (at your option) any later version.        *
*                                                                             *
*   The GNU C Library is distributed in the hope that it will be useful,      *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
*   Lesser General Public License for more details.                           *
*                                                                             *
*   You should have received a copy of the GNU Lesser General Public          *
*   License along with the GNU C Library; if not, write to the Free           *
*   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA         *
*   02111-1307 USA.                                                           *
******************************************************************************/
#include "config.h"
#include "webmon.h"
#include "common.h"
#include "log.h"

/******************************************************************************
 *                                                                            *
 * Function: webmon_init_kv_string                                               *
 *                                                                            *
 * Purpose: init the kv object                                            *
 *                                                                            *
 * Return value: ptr to a init kv object                                  *
 *                                                                            *
 ******************************************************************************/
webmon_kv_string_t *webmon_init_kv_string() {
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    webmon_kv_string_t *kv = zbx_malloc(NULL, sizeof(webmon_kv_string_t));
    kv->value = NULL;
    kv->key = NULL;
    kv->next = NULL;

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return kv;
}


/******************************************************************************
 *                                                                            *
 * Function: webmon_init_str_string                                           *
 *                                                                            *
 * Purpose: init the str object                                               *
 *                                                                            *
 * Return value: ptr to a init str object                                     *
 *                                                                            *
 ******************************************************************************/
webmon_str_string_t *webmon_init_str_string() {
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    webmon_str_string_t *str = zbx_malloc(NULL, sizeof(webmon_str_string_t));
    str->value = NULL;
    str->next = NULL;

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return str;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_init_config                                               *
 *                                                                            *
 * Purpose: init the config object                                            *
 *                                                                            *
 * Return value: ptr to a init config object                                  *
 *                                                                            *
 ******************************************************************************/
config_t *webmon_init_config() {
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    config_t *config = zbx_malloc(NULL, sizeof(config_t));
    config->site_name = NULL;
    config->first_step = NULL;

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return config;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_init_execution                                            *
 *                                                                            *
 * Purpose: init a execution object                                           *
 *                                                                            *
 * Return value: ptr to a execution step object                               *
 *                                                                            *
 ******************************************************************************/
execution_t *webmon_init_execution()
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    execution_t *exec = zbx_malloc(NULL, sizeof(execution_t));
    exec->next       = NULL;
    exec->config     = NULL;
    exec->config_key = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return exec;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_init_step                                                 *
 *                                                                            *
 * Purpose: init a step object                                                *
 *                                                                            *
 * Return value: ptr to a init step object                                    *
 *                                                                            *
 ******************************************************************************/
step_t *webmon_init_step(step_t *prev)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    step_t *step = zbx_malloc(NULL, sizeof(step_t));
    step->result = webmon_init_step_response();
    step->method = "GET";
    step->custommethod = 0;
    step->timeout_s = 20L;
    step->headers_list = NULL;
    step->next = NULL;
    step->url  = NULL;
    step->request_body = NULL;
    step->variable_list = NULL;
    step->prev=prev;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return step;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_init_step_response                                        *
 *                                                                            *
 * Purpose: init a step-response object                                       *
 *                                                                            *
 * Return value: ptr to a init step-response object                           *
 *                                                                            *
 ******************************************************************************/
webmon_step_response_t *webmon_init_step_response()
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    webmon_step_response_t *response = zbx_malloc(NULL, sizeof(webmon_step_response_t));
    response->response = NULL;
    response->size = 0;
    response->method = NULL;
    response->url = NULL;
    response->scheme = NULL;
    response->local_ip = NULL;
    response->primary_ip = NULL;
    response->strerror = NULL;
    response->response_code = 0;
    response->result_variable_list = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return response;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_config                                               *
 *                                                                            *
 * Purpose: free the config object                                            *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_free_config(config_t *config)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(config->site_name != NULL) {
        zbx_free(config->site_name);
        config->site_name = NULL;
    }
    step_t *step=config->first_step;
    while(step) {
        step_t *step_s=step->next;
        webmon_free_step(step);
        step=step_s;
    }
    zbx_free(config);
    config = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_str_string                                                 *
 *                                                                            *
 * Purpose: free the str object                                              *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int *webmon_free_str_string(webmon_str_string_t *str)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    if(str->next != NULL) {
        webmon_free_str_string(str->next);
    }
    if(str->value != NULL) {
        zbx_free(str->value);
        str->value = NULL;
    }
    zbx_free(str);
    str = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_kv_string                                                 *
 *                                                                            *
 * Purpose: free the kv object                                              *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int *webmon_free_kv_string(webmon_kv_string_t *kv)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    zabbix_log(LOG_LEVEL_DEBUG, "Free Key=%s, Value=%s", kv->key, kv->value);
    if(kv->next != NULL) {
        webmon_free_kv_string(kv->next);
        kv->next = NULL;
    }
    zbx_free(kv->value);
    zbx_free(kv->key);
    zbx_free(kv);

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_step                                                 *
 *                                                                            *
 * Purpose: free the step object                                              *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_free_step(step_t *step)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(step->result != NULL) {
        webmon_free_step_response(step->result);
    }
    if(step->headers_list != NULL) {
        webmon_free_str_string(step->headers_list);
    }
    if(step->request_body != NULL) {
        zbx_free(step->request_body);
        step->request_body = NULL;
    }
    if(step->custommethod) {
        zbx_free(step->method);
        step->method = NULL;
    }
    if(step->variable_list) {
        webmon_free_kv_string(step->variable_list);
    }
    zbx_free(step->url);
    step->url = NULL;
    zbx_free(step);
    step = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_step_response                                        *
 *                                                                            *
 * Purpose: free the step-response object                                     *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_free_step_response(webmon_step_response_t *response)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(response->response != NULL) {
        zbx_free(response->response);
        response->response = NULL;
    }
    if(response->result_variable_list != NULL) {
        webmon_free_kv_string(response->result_variable_list);
        response->result_variable_list = NULL;
    }
    if(response->url != NULL) {
        zbx_free(response->url);
        response->url = NULL;
    }
    if(response->method != NULL) {
        zbx_free(response->method);
        response->method = NULL;
    }
    if(response->scheme != NULL) {
        zbx_free(response->scheme);
        response->scheme = NULL;
    }
    if(response->primary_ip != NULL) {
        zbx_free(response->primary_ip);
        response->primary_ip = NULL;
    }
    if(response->local_ip != NULL) {
        zbx_free(response->local_ip);
        response->local_ip = NULL;
    }
    if(response->strerror != NULL) {
        zbx_free(response->strerror);
        response->strerror = NULL;
    }
    zbx_free(response);
    response = NULL;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_execution                                        *
 *                                                                            *
 * Purpose: free the step-response object                                     *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_free_execution(execution_t *executions)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(executions->next != NULL) {
        webmon_free_execution(executions->next);
        executions->next = NULL;
    }
    if(executions->config != NULL) {
        webmon_free_config(executions->config);
        executions->config = NULL;
    }
    if(executions->config_key != NULL) {
        zbx_free(executions->config_key);
        executions->config_key = NULL;
    }
    zbx_free(executions);
    executions = NULL;

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_free_one_execution                                        *
 *                                                                            *
 * Purpose: free the executions object                                        *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_free_one_execution(execution_t *executions)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(executions->config != NULL) {
        webmon_free_config(executions->config);
    }
    if(executions->config_key != NULL) {
        zbx_free(executions->config_key);
    }
    zbx_free(executions);
    executions = NULL;

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_add_execution                                             *
 *                                                                            *
 * Purpose: add an execution and return it                                    *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
execution_t *webmon_add_execution(execution_t* executions, config_t *config, char *config_key)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    execution_t *new_execution = webmon_init_execution();
    new_execution->config = config;
    new_execution->next   = executions;
    new_execution->config_key = config_key;


    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return new_execution;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_get_step_in_exec                                          *
 *                                                                            *
 * Purpose: search in execution the step identifed by config_key and step_key *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
step_t *webmon_get_step_in_exec(execution_t *exec, char *config_key, int step_key)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    execution_t *ptr_exec = exec;
    config_t *ptr_config;
    step_t *ptr_step;
    int i=1;

    while(ptr_exec != NULL)
    {
        if(strcmp(ptr_exec->config_key, config_key) == 0) {
            break;
        }
        ptr_exec = ptr_exec->next;
    }
    if(ptr_exec == NULL)
    {
        return NULL;
    }
    ptr_config = ptr_exec->config;
    if(ptr_config == NULL) {
        return NULL;
    }
    ptr_step = ptr_config->first_step;
    while(ptr_step != NULL)
    {
        if(i == step_key) {
            break;
        }
        ptr_step = ptr_step->next;
        i++;
    }

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return ptr_step;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_clean_exec                                                *
 *                                                                            *
 * Purpose: keep only 2 instance of config with key config_data               *
 *                                                                            *
 * Return value: SYSINFO_RET_OK if everythin OK                               *
 *                                                                            *
 ******************************************************************************/
int webmon_clean_exec(execution_t* exec, char *config_key)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    execution_t *ptr_exec = exec;
    execution_t *ptr_prev = NULL;

    int i=0;

    while(ptr_exec != NULL)
    {
        execution_t *next=ptr_exec->next;
        if(strcmp(ptr_exec->config_key, config_key) == 0)
        {
            i++;
        }

        if ((i>2) && (strcmp(ptr_exec->config_key, config_key) == 0))
        {
                ptr_prev->next = next;
                webmon_free_one_execution(ptr_exec);
        } else {
            ptr_prev = ptr_exec;
        }

        ptr_exec = next;
    }

    // return OK
    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}
