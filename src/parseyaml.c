/******************************************************************************
*   Copyright (C) 2021 Mathieu Goulin <mathieu.goulin@gadz.org>               *
*                                                                             *
*   This program is free software; you can redistribute it and/or             *
*   modify it under the terms of the GNU Lesser General Public                *
*   License as published by the Free Software Foundation; either              *
*   version 2.1 of the License, or (at your option) any later version.        *
*                                                                             *
*   The GNU C Library is distributed in the hope that it will be useful,      *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
*   Lesser General Public License for more details.                           *
*                                                                             *
*   You should have received a copy of the GNU Lesser General Public          *
*   License along with the GNU C Library; if not, write to the Free           *
*   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA         *
*   02111-1307 USA.                                                           *
******************************************************************************/
#include "config.h"
#include "webmon.h"
#include "log.h"
#include "common.h"


/******************************************************************************
 *                                                                            *
 * Function: webmon_parse_next                                                *
 *                                                                            *
 * Purpose: Find in yaml document the next event                              *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_parse_next( yaml_parser_t* parser, yaml_event_t* event )
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    /* Parse next scalar. if wrong exit with error */
    if ( !yaml_parser_parse( parser, event ) )
    {
        zabbix_log(LOG_LEVEL_ERR, "Parser error %d\n", parser->error );
        return -1;
    }

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_parse_yaml                                                *
 *                                                                            *
 * Purpose: Populate the config with content of config_input FILE             *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_parse_yaml(config_t *config, FILE *config_input)
{
    yaml_parser_t parser;
    yaml_event_t event;

    unsigned int map_seq = WEBMON_MAPPING_CONFIG_OBJECT_ID; /* Index of mapping inside sequence */

    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    /* Initialize parser */
    if(!yaml_parser_initialize(&parser))
    {
        zabbix_log(LOG_LEVEL_ERR, "Failed to initialize parser");

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return 1;
    }
    /* Set input file */
    yaml_parser_set_input_file(&parser, config_input);

    /* BEGIN Parsing */
    do
    {
        webmon_parse_next( &parser, &event ); /* Parse new event */

        /* Decide what to do with each event */
        if(webmon_parse_event_switch( &map_seq, config, &parser, &event) != 0)
        {
            zabbix_log(LOG_LEVEL_ERR, "Failed to parse file");

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return 1;
        }

        if ( event.type != YAML_STREAM_END_EVENT )
        {
            yaml_event_delete( &event );
        }
    }
    while ( event.type != YAML_STREAM_END_EVENT );
    yaml_event_delete(&event);
    yaml_parser_delete(&parser);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}


/******************************************************************************
 *                                                                            *
 * Function: webmon_parse_to_data                                             *
 *                                                                            *
 * Purpose: When fin data it can be single value or a subsequence/object      *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_parse_to_data( unsigned int* map_seq, config_t* config, yaml_parser_t* parser, yaml_event_t* event)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    char* key = (char*) strdup((char *)event->data.scalar.value);
    yaml_event_delete( event );
    webmon_parse_next( parser, event );
    switch(event->type)
    {
        case YAML_SEQUENCE_START_EVENT:
            zabbix_log(LOG_LEVEL_DEBUG,"(%d) SEQUENCE START : %s", *map_seq, key);
            if(strcmp(key, WEBMON_STEP_KEY) == 0 ) {
                *map_seq = WEBMON_MAPPING_STEP_OBJECT_ID;
            }
            if(strcmp(key, WEBMON_HEADERS_KEY) == 0 ) {
                *map_seq = WEBMON_MAPPING_HEADERS_OBJECT_ID;
            }
            if(strcmp(key, WEBMON_VARIABLES_KEY) == 0 ) {
                *map_seq = WEBMON_MAPPING_VARIABLE_OBJECT_ID;
            }
            break;
        case YAML_SCALAR_EVENT:
            zabbix_log(LOG_LEVEL_DEBUG,"(%d) STORE %s=%s in objecttype=%d", *map_seq, key, (char *) event->data.scalar.value, *map_seq);
            webmon_update_config(*map_seq, key, strdup((char *)event->data.scalar.value), config);

            break;
        default:
            break;
    }
    zbx_free(key);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_find_last_step                                            *
 *                                                                            *
 * Purpose: Search the last object in a list                                  *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
step_t *webmon_find_last_step(config_t *config)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(config->first_step == NULL) {
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return NULL;
    }
    step_t *step = config->first_step;
    while(step->next) {
        step = step->next;
    }
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return step;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_update_config                                             *
 *                                                                            *
 * Purpose: Update the config object                                          *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_update_config(int obj_type, char *key, char *value, config_t* config)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    // config.site_name
    if(obj_type == WEBMON_MAPPING_CONFIG_OBJECT_ID && strcmp(key, WEBMON_SITENAME_KEY) == 0) {
        config->site_name = strdup(value);
    }
    // config.step[].url
    if(obj_type == WEBMON_MAPPING_STEP_OBJECT_ID && strcmp(key, WEBMON_URL_KEY) == 0) {
       step_t* step = webmon_find_last_step(config);
       step->url = strdup(value);
    }
    if(obj_type == WEBMON_MAPPING_STEP_OBJECT_ID && strcmp(key, WEBMON_CUSTOMREQUEST_KEY) == 0) {
       step_t* step = webmon_find_last_step(config);
       step->method = strdup(value);
       step->custommethod = 1;
    }
    if(obj_type == WEBMON_MAPPING_STEP_OBJECT_ID && strcmp(key, WEBMON_REQBODY_KEY) == 0) {
       step_t* step = webmon_find_last_step(config);
       step->request_body = strdup(value);
    }
    if(obj_type == WEBMON_MAPPING_STEP_OBJECT_ID && strcmp(key, WEBMON_TIMEOUT_KEY) == 0) {
       step_t* step = webmon_find_last_step(config);
       step->timeout_s = atol(value);
    }
    if(obj_type == WEBMON_MAPPING_HEADERS_OBJECT_ID) {
       step_t* step = webmon_find_last_step(config);
       webmon_str_string_t *str = webmon_init_str_string();
       str->value = strdup(value);
       str->next = step->headers_list;
       step->headers_list = str;
    }
    if(obj_type == WEBMON_MAPPING_VARIABLE_OBJECT_ID) {
       step_t* step = webmon_find_last_step(config);
       webmon_kv_string_t *kv = webmon_init_kv_string();
       kv->value = strdup(value);
       kv->key = strdup(key);
       kv->next = step->variable_list;
       step->variable_list = kv;
    }

    zbx_free(value);
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
	return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_parse_event_switch                                        *
 *                                                                            *
 * Purpose:  process event                                                    *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_parse_event_switch( unsigned int* map_seq, config_t* config, yaml_parser_t* parser, yaml_event_t* event)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    switch(event->type)
    {
    /* Stream start/end */
    case YAML_STREAM_START_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) STREAM START", *map_seq);
        break;
    case YAML_STREAM_END_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) STREAM END", *map_seq);
        break;
    case YAML_DOCUMENT_START_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) DOCUMENT START", *map_seq);
        break;
    case YAML_DOCUMENT_END_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) DOCUMENT END", *map_seq);
        break;
    case YAML_ALIAS_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG, "(%d) ERROR: YAML Alias not supported: Got alias (anchor %s)", *map_seq, event->data.alias.anchor );
        return 1;
        break;
    case YAML_MAPPING_END_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) MAPPING END", *map_seq);
        break;
    case YAML_MAPPING_START_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG, "(%d) MAPPING START", *map_seq);
        if(*map_seq == WEBMON_MAPPING_STEP_OBJECT_ID) {
            zabbix_log(LOG_LEVEL_DEBUG,"(%d) Init step", *map_seq);
            step_t* step = webmon_find_last_step(config);
            if(step != NULL) {
                step->next = webmon_init_step(step);
            } else {
                config->first_step = webmon_init_step(NULL);
            }
        }
        break;
    case YAML_SEQUENCE_START_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) SEQUENCE START", *map_seq);
        break;
    case YAML_SEQUENCE_END_EVENT:
        zabbix_log(LOG_LEVEL_DEBUG,"(%d) SEQUENCE END", *map_seq);
        if(*map_seq == WEBMON_MAPPING_HEADERS_OBJECT_ID) {
            *map_seq = WEBMON_MAPPING_STEP_OBJECT_ID;
        } else if(*map_seq == WEBMON_MAPPING_STEP_OBJECT_ID) {
            *map_seq = WEBMON_MAPPING_CONFIG_OBJECT_ID;
        } else if(*map_seq == WEBMON_MAPPING_VARIABLE_OBJECT_ID) {
            *map_seq = WEBMON_MAPPING_STEP_OBJECT_ID;
        }

        break;
    case YAML_SCALAR_EVENT:
        if(webmon_parse_to_data( map_seq, config, parser, event) != 0)
        {
            zabbix_log(LOG_LEVEL_ERR, "Failed to parse yaml file (YAML_SCALAR_EVENT Error)");

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return 1;
        }
        break;
    case YAML_NO_EVENT:
        zabbix_log(LOG_LEVEL_ERR, "ERROR: YAML YAML_NO_EVENT receive ...");

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return 1;
        break;
    default:
        break;
    }

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}
