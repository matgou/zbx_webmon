/******************************************************************************
*   Copyright (C) 2021 Mathieu Goulin <mathieu.goulin@gadz.org>               *
*                                                                             *
*   This program is free software; you can redistribute it and/or             *
*   modify it under the terms of the GNU Lesser General Public                *
*   License as published by the Free Software Foundation; either              *
*   version 2.1 of the License, or (at your option) any later version.        *
*                                                                             *
*   The GNU C Library is distributed in the hope that it will be useful,      *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
*   Lesser General Public License for more details.                           *
*                                                                             *
*   You should have received a copy of the GNU Lesser General Public          *
*   License along with the GNU C Library; if not, write to the Free           *
*   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA         *
*   02111-1307 USA.                                                           *
******************************************************************************/
#include "config.h"
#include "webmon.h"
#include "log.h"
#include "common.h"

#include <curl/curl.h>


static size_t webmon_step_save_output(void *data, size_t size, size_t nmemb, void *userp);


/******************************************************************************
 *                                                                            *
 * Function: webmon_curl_populate_long_info                                    *
 *                                                                            *
 * Purpose: populate a long-field with a curl query result                     *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_curl_populate_long_info(CURL *curl, CURLINFO info, long *number)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    curl_easy_getinfo(curl, info, number);
    zabbix_log(LOG_LEVEL_DEBUG, "curl_easy_getinfo(curl, ...) : '%li'", *number);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_curl_populate_double_info                                    *
 *                                                                            *
 * Purpose: populate a double-field with a curl query result                     *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_curl_populate_double_info(CURL *curl, CURLINFO info, double *number)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    curl_easy_getinfo(curl, info, number);
    zabbix_log(LOG_LEVEL_DEBUG, "curl_easy_getinfo(curl, ...) : '%lf'", *number);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_curl_populate_str_info                                    *
 *                                                                            *
 * Purpose: populate a str-field with a curl query result                     *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_curl_populate_str_info(CURL *curl, CURLINFO info, char **ptr)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    char *str_value = NULL;
    curl_easy_getinfo(curl, info, &str_value);
    (*ptr) = strdup(str_value);
    zabbix_log(LOG_LEVEL_DEBUG, "curl_easy_getinfo(curl, ...) : '%s'", *ptr);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_step_query                                                *
 *                                                                            *
 * Purpose: Run query on step                                                 *
 *                                                                            *
 * Return value: 0 if everything OK                                           *
 *                                                                            *
 ******************************************************************************/
int webmon_step_query(step_t *step)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    CURL *curl;
    CURLcode res;

    /* In windows, this will init the winsock stuff */
    curl_global_init(CURL_GLOBAL_ALL);

    /* get a curl handle */
    curl = curl_easy_init();
    if(curl) {

        curl_easy_setopt(curl, CURLOPT_URL, step->url);
        /* handle result */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, webmon_step_save_output);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, step);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, step->method);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, step->timeout_s);

        /* Now specify the Header data */
        webmon_str_string_t *header = step->headers_list;
        struct curl_slist *curl_header_list = NULL;
        while(header != NULL) {
            curl_header_list = curl_slist_append(curl_header_list, header->value);
            zabbix_log(LOG_LEVEL_DEBUG, "header: %s", webmon_replace_vars(step->prev, header->value));
            header=header->next;
        }
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, curl_header_list);
        /* Now specify the POST data */
        if(step->request_body != NULL) {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, step->request_body);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(step->request_body));
        }

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK) {
            zabbix_log(LOG_LEVEL_ERR, "curl_easy_perform() failed: %s", curl_easy_strerror(res));
            step->result->strerror = strdup(curl_easy_strerror(res));
            if(curl_header_list != NULL) {
                curl_slist_free_all(curl_header_list);
            }

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return 1;
        } else {
            // str flow
            webmon_curl_populate_str_info(curl, CURLINFO_EFFECTIVE_URL, &(step->result->url));
            zabbix_log(LOG_LEVEL_DEBUG, "url=%s", step->result->url);
            webmon_curl_populate_str_info(curl, CURLINFO_SCHEME, &(step->result->scheme));
            webmon_curl_populate_str_info(curl, CURLINFO_PRIMARY_IP, &(step->result->primary_ip));
            webmon_curl_populate_str_info(curl, CURLINFO_LOCAL_IP, &(step->result->local_ip));
            // long
            webmon_curl_populate_long_info(curl, CURLINFO_RESPONSE_CODE, &(step->result->response_code));
            webmon_curl_populate_long_info(curl, CURLINFO_HTTP_VERSION, &(step->result->http_version));
            webmon_curl_populate_long_info(curl, CURLINFO_OS_ERRNO, &(step->result->error_no));
            webmon_curl_populate_long_info(curl, CURLINFO_PRIMARY_PORT, &(step->result->primary_port));
            webmon_curl_populate_long_info(curl, CURLINFO_LOCAL_PORT, &(step->result->local_port));
            // double
            webmon_curl_populate_double_info(curl, CURLINFO_TOTAL_TIME, &(step->result->total_time));
            webmon_curl_populate_double_info(curl, CURLINFO_NAMELOOKUP_TIME, &(step->result->namelookup_time));
            webmon_curl_populate_double_info(curl, CURLINFO_CONNECT_TIME, &(step->result->connect_time));
            webmon_curl_populate_double_info(curl, CURLINFO_APPCONNECT_TIME, &(step->result->appconnect_time));
            webmon_curl_populate_double_info(curl, CURLINFO_PRETRANSFER_TIME, &(step->result->pretransfer_time));
            webmon_curl_populate_double_info(curl, CURLINFO_STARTTRANSFER_TIME, &(step->result->starttransfert_time));
            webmon_curl_populate_double_info(curl, CURLINFO_REDIRECT_TIME, &(step->result->redirect_time));
            webmon_curl_populate_double_info(curl, CURLINFO_SIZE_UPLOAD, &(step->result->upload_size));
            webmon_curl_populate_double_info(curl, CURLINFO_SIZE_DOWNLOAD, &(step->result->download_size));
            webmon_curl_populate_double_info(curl, CURLINFO_SPEED_DOWNLOAD, &(step->result->download_speed));
            webmon_curl_populate_double_info(curl, CURLINFO_SPEED_UPLOAD, &(step->result->upload_speed));

            step->result->strerror = strdup("No Error: step is OK");

            webmon_kv_string_t *vars = step->variable_list;
            while(vars != NULL) {
                zabbix_log(LOG_LEVEL_DEBUG, "Calculate '%s' as '%s'", vars->key, vars->value);
                char *res = webmon_exec_lua(vars->value, step->url, step->result->response);

                webmon_kv_string_t *result_vars = webmon_init_kv_string();
                result_vars->key = strdup(vars->key);
                result_vars->value = res;
                result_vars->next = step->result->result_variable_list;
                step->result->result_variable_list = result_vars;
                zabbix_log(LOG_LEVEL_DEBUG, "Push Key=%s, Value=%s", result_vars->key, result_vars->value);
                vars = vars->next;
            }
        }

        if(curl_header_list != NULL) {
            curl_slist_free_all(curl_header_list);
        }
        /* always cleanup */
        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_step_save_output                                          *
 *                                                                            *
 * Purpose: Save in memory output                                             *
 *                                                                            *
 * Return value: size of current buffer                                       *
 *                                                                            *
 ******************************************************************************/
 static size_t webmon_step_save_output(void *data, size_t size, size_t nmemb, void *userp)
 {
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    size_t realsize = size * nmemb;

    webmon_step_response_t *response = ((step_t *) userp)->result;
    response->size     = response->size + realsize;
    if(response->response == NULL) {
        response->response = strdup(data);
    } else {
        char* body = (char *) malloc(response->size);
        zbx_snprintf(body, response->size,"%s%s", response->response, (char *) data);
        zbx_free(response->response);
        response->response = body;
    }

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return realsize;
 }
