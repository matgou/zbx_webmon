/******************************************************************************
*   Copyright (C) 2021 Mathieu Goulin <mathieu.goulin@gadz.org>               *
*                                                                             *
*   This program is free software; you can redistribute it and/or             *
*   modify it under the terms of the GNU Lesser General Public                *
*   License as published by the Free Software Foundation; either              *
*   version 2.1 of the License, or (at your option) any later version.        *
*                                                                             *
*   The GNU C Library is distributed in the hope that it will be useful,      *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
*   Lesser General Public License for more details.                           *
*                                                                             *
*   You should have received a copy of the GNU Lesser General Public          *
*   License along with the GNU C Library; if not, write to the Free           *
*   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA         *
*   02111-1307 USA.                                                           *
******************************************************************************/
#ifndef webmon_h
#define webmon_h
#include "../zabbix/include/module.h"
#include <yaml.h>

/* Mapping yaml */
#define WEBMON_MAPPING_CONFIG_OBJECT_ID 0
#define WEBMON_MAPPING_STEP_OBJECT_ID 1
#define WEBMON_MAPPING_HEADERS_OBJECT_ID 2
#define WEBMON_MAPPING_VARIABLE_OBJECT_ID 3

#define WEBMON_STEP_KEY             "steps"
#define WEBMON_SITENAME_KEY         "site_name"
#define WEBMON_URL_KEY              "url"
#define WEBMON_CUSTOMREQUEST_KEY    "method"
#define WEBMON_TIMEOUT_KEY          "timeout"
#define WEBMON_HEADERS_KEY          "headers"
#define WEBMON_REQBODY_KEY          "request_body"
#define WEBMON_VARIABLES_KEY        "variables"

/* config struct */

typedef struct
{
    char *value;
    void *next;
} webmon_str_string_t;

typedef struct
{
    char *key;
    char *value;
    void *next;
} webmon_kv_string_t;

 typedef struct {
   char *response;
   char *strerror;
   unsigned long size;
   char *method; /* CURLINFO_EFFECTIVE_METHOD */ // not implemented
   char *url; /* CURLINFO_EFFECTIVE_URL */
   char *scheme; /* CURLINFO_SCHEME */
   long response_code; /* CURLINFO_RESPONSE_CODE */
   long http_version; /* CURLINFO_HTTP_VERSION */
   double total_time; /* CURLINFO_TOTAL_TIME */
   double namelookup_time; /* CURLINFO_NAMELOOKUP_TIME  */
   double connect_time; /* CURLINFO_CONNECT_TIME */
   double appconnect_time; /* CURLINFO_APPCONNECT_TIME  */
   double pretransfer_time;/* CURLINFO_PRETRANSFER_TIME  */
   double starttransfert_time; /* CURLINFO_STARTTRANSFER_TIME  */
   double redirect_time; /* CURLINFO_REDIRECT_TIME */
   double upload_size; /* CURLINFO_SIZE_UPLOAD */
   double download_size; /* CURLINFO_SIZE_DOWNLOAD */
   double download_speed; /* CURLINFO_SPEED_DOWNLOAD  */
   double upload_speed; /* CURLINFO_SPEED_UPLOAD */
   long error_no; /* CURLINFO_OS_ERRNO */
   char *primary_ip; /* CURLINFO_PRIMARY_IP */
   long primary_port; /* CURLINFO_PRIMARY_PORT */
   char *local_ip; /* CURLINFO_LOCAL_IP */
   long local_port; /* CURLINFO_LOCAL_PORT */
   webmon_kv_string_t *result_variable_list;
 } webmon_step_response_t;

typedef struct
{
    void *next;
    void *prev;
    char *url;
    char *method;
    int custommethod;
    int timeout_s; /* Step timeout in second */
    char *request_body;
    webmon_kv_string_t *variable_list;
    webmon_str_string_t *headers_list;
    webmon_step_response_t *result;
} step_t;

typedef struct
{
    char *site_name;
    step_t *first_step;
} config_t;

typedef struct
{
    config_t *config;
    char *config_key;
    void *next;
} execution_t;

/* Function definition */
int	webmon_query(AGENT_REQUEST *request, AGENT_RESULT *result);
int	webmon_internal_mem_plot_usage(AGENT_REQUEST *request, AGENT_RESULT *result);
int webmon_get_body(AGENT_REQUEST *request, AGENT_RESULT *result);
int webmon_get_stepinfo(AGENT_REQUEST *request, AGENT_RESULT *result);


int webmon_parse_event_switch( unsigned int* map_seq, config_t* config, yaml_parser_t* parser, yaml_event_t* event);
step_t *webmon_get_step_in_exec(execution_t *exec, char *config_key, int step_key);
int webmon_parse_to_data( unsigned int* map_seq, config_t* config, yaml_parser_t* parser, yaml_event_t* event);
int webmon_parse_next( yaml_parser_t* parser, yaml_event_t* event );
int webmon_parse_yaml(config_t *config, FILE *config_input);
step_t *webmon_init_step();
step_t *webmon_find_last_step(config_t *config);
int webmon_update_config(int obj_type, char *key, char *value, config_t* config);
int webmon_run(config_t *config, char *config_key);
int webmon_free_step(step_t *step);
int webmon_free_config(config_t *config);
int webmon_free_step_response(webmon_step_response_t *response);
int webmon_clean_exec(execution_t* executions, char *config_data);
webmon_step_response_t *webmon_init_step_response();
config_t *webmon_init_config();
step_t *webmon_init_step(step_t *prev);
int webmon_step_query(step_t *step);
execution_t *webmon_add_execution(execution_t* executions, config_t *config, char *config_key);
execution_t *webmon_init_execution();
int webmon_free_execution(execution_t *executions);
webmon_str_string_t *webmon_init_str_string();
int *webmon_free_str_string(webmon_str_string_t *str);
webmon_kv_string_t *webmon_init_kv_string();
int *webmon_free_kv_string(webmon_kv_string_t *str);
char *webmon_exec_lua(char *code, const char *url, const char *body);
int webmon_get_vars(AGENT_REQUEST *request, AGENT_RESULT *result);
char *webmon_replace_vars(step_t *var_repo, char *value_template);
#endif
