/******************************************************************************
*   Copyright (C) 2021 Mathieu Goulin <mathieu.goulin@gadz.org>               *
*                                                                             *
*   This program is free software; you can redistribute it and/or             *
*   modify it under the terms of the GNU Lesser General Public                *
*   License as published by the Free Software Foundation; either              *
*   version 2.1 of the License, or (at your option) any later version.        *
*                                                                             *
*   The GNU C Library is distributed in the hope that it will be useful,      *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
*   Lesser General Public License for more details.                           *
*                                                                             *
*   You should have received a copy of the GNU Lesser General Public          *
*   License along with the GNU C Library; if not, write to the Free           *
*   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA         *
*   02111-1307 USA.                                                           *
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "webmon.h"
#include "log.h"
#include "common.h"
#include "config.h"


/* the variable keeps timeout setting for item processing */
static int	item_timeout = 10;

/* this variable store execution */
static execution_t **executions = NULL;

/* this variable store lua state */
lua_State *L = NULL;

/* module SHOULD define internal functions as static and use a naming pattern different from Zabbix internal */
/* symbols (zbx_*) and loadable module API functions (zbx_module_*) to avoid conflicts                       */
static ZBX_METRIC keys[] =
    /*	KEY			                    FLAG		    FUNCTION	                    TEST PARAMETERS */
{
    {"webmon.query",	                CF_HAVEPARAMS,	webmon_query,	                NULL},
    {"webmon.internal.mem_plot_usage",	0,	            webmon_internal_mem_plot_usage,	"website_scenario.yml"},
    {"webmon.get.body.data",            CF_HAVEPARAMS,	webmon_get_body,	            "website_scenario.yml,2"},
    {"webmon.get.step_info",            CF_HAVEPARAMS,	webmon_get_stepinfo,            "website_scenario.yml,2,<info>"},
    {"webmon.get.vars.value",           CF_HAVEPARAMS,  webmon_get_vars,                "website_scenario.yml,2,<key>"},
    {NULL}
};

/******************************************************************************
 *                                                                            *
 * Function: zbx_module_api_version                                           *
 *                                                                            *
 * Purpose: returns version number of the module interface                    *
 *                                                                            *
 * Return value: ZBX_MODULE_API_VERSION - version of module.h module is       *
 *               compiled with, in order to load module successfully Zabbix   *
 *               MUST be compiled with the same version of this header file   *
 *                                                                            *
 ******************************************************************************/
int	zbx_module_api_version(void)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return ZBX_MODULE_API_VERSION;
}

/******************************************************************************
 *                                                                            *
 * Function: zbx_module_item_timeout                                          *
 *                                                                            *
 * Purpose: set timeout value for processing of items                         *
 *                                                                            *
 * Parameters: timeout - timeout in seconds, 0 - no timeout set               *
 *                                                                            *
 ******************************************************************************/
void	zbx_module_item_timeout(int timeout)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    item_timeout = timeout;
}

/******************************************************************************
 *                                                                            *
 * Function: zbx_module_item_list                                             *
 *                                                                            *
 * Purpose: returns list of item keys supported by the module                 *
 *                                                                            *
 * Return value: list of item keys                                            *
 *                                                                            *
 ******************************************************************************/
ZBX_METRIC	*zbx_module_item_list(void)
{
	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return keys;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_internal_mem_plot_usage                                   *
 *                                                                            *
 * Purpose: returns number of execution                                       *
 *                                                                            *
 * Return value: SYSINFO_RET_OK if everything is OK                           *
 *                                                                            *
 ******************************************************************************/
int	webmon_internal_mem_plot_usage(AGENT_REQUEST *request, AGENT_RESULT *result)
{
    int i = 0;

	zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    execution_t *exec = *executions;
    while(exec != NULL)
    {
        exec = exec->next;
        i++;
    }
	SET_UI64_RESULT(result, i);

	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
	return SYSINFO_RET_OK;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_query                                                     *
 *                                                                            *
 * Purpose: handle request, load yaml and execute a query                     *
 *                                                                            *
 * Return value: Query status                                                 *
 *                                                                            *
 ******************************************************************************/
int	webmon_query(AGENT_REQUEST *request, AGENT_RESULT *result)
{
    char	*config_data;
    int	rc = 0;
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if (1 != request->nparam)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number of parameters."));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }


    config_data = get_rparam(request, 0);
    config_t *config = webmon_init_config();

    /* Set a file input. */
    FILE *config_input = fopen(config_data, "rb");
    if(config_input == NULL)
    {
        webmon_free_config(config);
        zabbix_log(LOG_LEVEL_ERR, "Error, unable to open file : %s", config_data);
        SET_MSG_RESULT(result, strdup("unable to open file"));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    if(webmon_parse_yaml(config, config_input) != 0)
    {
        fclose(config_input);
        webmon_free_config(config);
        SET_MSG_RESULT(result, strdup("fail to parse config"));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    if(webmon_run(config, config_data) != 0)
    {
        fclose(config_input);
        webmon_free_config(config);
        SET_MSG_RESULT(result, strdup("fail to execute config"));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    if(webmon_clean_exec(*executions, config_data) != 0)
    {
        fclose(config_input);
        SET_MSG_RESULT(result, strdup("fail to execute config"));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }


    /* Cleanup */
    fclose(config_input);
    SET_UI64_RESULT(result, rc);

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return SYSINFO_RET_OK;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_get_stepinfo                                              *
 *                                                                            *
 * Purpose: get info about the step                                           *
 *                                                                            *
 * Return value: SYSINFO_RET_OK if everythin OK                               *
 *                                                                            *
 ******************************************************************************/
int webmon_get_stepinfo(AGENT_REQUEST *request, AGENT_RESULT *result)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    if (3 != request->nparam)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number of parameters : <scenario>,<no_step>,<type_info>"));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }

    char *config_key = get_rparam(request, 0);
    int step_key = atoi(get_rparam(request, 1));
    char *info_key = get_rparam(request, 2);
    if(step_key == 0)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number in step_key param."));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    /* Find step in memory */
    step_t *step = webmon_get_step_in_exec(*executions, config_key, step_key);
    if (step == NULL)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid arguments unable to find the run or the step."));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    if (step->result == NULL)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Impossible step->result is null"));

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }

    zabbix_log(LOG_LEVEL_WARNING, "Get : %s", info_key );
    if(strcmp(info_key, "body.size") == 0) {
        if(step->result->response != NULL) {
            SET_UI64_RESULT(result, step->result->size);

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
    } else if (strcmp(info_key, "strerror") == 0) {
        if(step->result->strerror != NULL) {
            SET_STR_RESULT(result, strdup(step->result->strerror));

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
    } else if (strcmp(info_key, "url") == 0) {
        if(step->result->url != NULL) {
            SET_STR_RESULT(result, strdup(step->result->url));

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
        zabbix_log(LOG_LEVEL_ERR, "Get : url, but it's null %s", info_key );
    } else if (strcmp(info_key, "scheme") == 0) {
        if(step->result->scheme != NULL) {
            SET_STR_RESULT(result, strdup(step->result->scheme));

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
    } else if (strcmp(info_key, "primary_ip") == 0) {
        if(step->result->primary_ip != NULL) {
            SET_STR_RESULT(result, strdup(step->result->primary_ip));

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
    } else if (strcmp(info_key, "local_ip") == 0) {
        if(step->result->local_ip != NULL) {
            SET_STR_RESULT(result, strdup(step->result->local_ip));

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
    } else if (strcmp(info_key, "response_code") == 0) {
        SET_UI64_RESULT(result, step->result->response_code);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "http_version") == 0) {
        SET_UI64_RESULT(result, step->result->http_version);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "error_no") == 0) {
        SET_UI64_RESULT(result, step->result->error_no);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "primary_port") == 0) {
        SET_UI64_RESULT(result, step->result->primary_port);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "local_port") == 0) {
        SET_UI64_RESULT(result, step->result->local_port);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "total_time") == 0) {
        SET_DBL_RESULT(result, step->result->total_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "namelookup_time") == 0) {
        SET_DBL_RESULT(result, step->result->namelookup_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "connect_time") == 0) {
        SET_DBL_RESULT(result, step->result->connect_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "appconnect_time") == 0) {
        SET_DBL_RESULT(result, step->result->appconnect_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "pretransfer_time") == 0) {
        SET_DBL_RESULT(result, step->result->pretransfer_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "starttransfert_time") == 0) {
        SET_DBL_RESULT(result, step->result->starttransfert_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "redirect_time") == 0) {
        SET_DBL_RESULT(result, step->result->redirect_time);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "upload_size") == 0) {
        SET_DBL_RESULT(result, step->result->upload_size);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "download_size") == 0) {
        SET_DBL_RESULT(result, step->result->download_size);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "download_speed") == 0) {
        SET_DBL_RESULT(result, step->result->download_speed);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    } else if (strcmp(info_key, "upload_speed") == 0) {
        SET_DBL_RESULT(result, step->result->upload_speed);

        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_OK;
    }
    SET_MSG_RESULT(result, strdup("Invalid param 3 : type_info unknow."));
    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return SYSINFO_RET_FAIL;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_get_body                                                  *
 *                                                                            *
 * Purpose: get body of a step                                                *
 *                                                                            *
 * Return value: SYSINFO_RET_OK if everythin OK                                            *
 *                                                                            *
 ******************************************************************************/
int webmon_get_body(AGENT_REQUEST *request, AGENT_RESULT *result)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    if (2 != request->nparam)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number of parameters."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }

    char *config_key = get_rparam(request, 0);
    int step_key = atoi(get_rparam(request, 1));
    if(step_key == 0)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number in step_key param."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    /* Find step in memory */
    step_t *step = webmon_get_step_in_exec(*executions, config_key, step_key);
    if (step == NULL)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid arguments unable to find the run or the step."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    if(step->result->response == NULL) {
        SET_TEXT_RESULT(result, strdup(""));
    } else {
        SET_TEXT_RESULT(result, strdup(step->result->response));
    }
    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return SYSINFO_RET_OK;
}


/******************************************************************************
 *                                                                            *
 * Function: webmon_get_vars                                                  *
 *                                                                            *
 * Purpose: get var-value of a step                                                *
 *                                                                            *
 * Return value: SYSINFO_RET_OK if everythin OK                                            *
 *                                                                            *
 ******************************************************************************/
int webmon_get_vars(AGENT_REQUEST *request, AGENT_RESULT *result)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if (3 != request->nparam)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number of parameters."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }

    char *config_key = get_rparam(request, 0);
    int step_key = atoi(get_rparam(request, 1));
    char *vars_key = get_rparam(request, 2);
    if(step_key == 0)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid number in step_key param."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    /* Find step in memory */
    step_t *step = webmon_get_step_in_exec(*executions, config_key, step_key);
    if (step == NULL)
    {
        /* set optional error message */
        SET_MSG_RESULT(result, strdup("Invalid arguments unable to find the run or the step."));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        return SYSINFO_RET_FAIL;
    }
    webmon_kv_string_t *vars_result = step->result->result_variable_list;
    while(vars_result != NULL) {
        if(strcmp(vars_result->key, vars_key) == 0) {
            SET_TEXT_RESULT(result, strdup(vars_result->value));
            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return SYSINFO_RET_OK;
        }
        vars_result = vars_result->next;
    }
    SET_MSG_RESULT(result, strdup("Invalid arguments unable to find the vars."));

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return SYSINFO_RET_FAIL;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_run                                                       *
 *                                                                            *
 * Purpose: run a scenario execution                                          *
 *                                                                            *
 * Return value: 0 if everythin OK                                            *
 *                                                                            *
 ******************************************************************************/
int webmon_run(config_t *config, char *config_key)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    zabbix_log(LOG_LEVEL_WARNING, "EXEC Scenario : %s", config->site_name );
    step_t *step = config->first_step;
    while(step != NULL) {
        webmon_step_query(step);
        zabbix_log(LOG_LEVEL_DEBUG, " => Body Size: %zu", step->result->size );
        step=step->next;
    }

    *executions = webmon_add_execution(*executions, config, strdup(config_key));

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return 0;
}


/******************************************************************************
 *                                                                            *
 * Function: zbx_module_init                                                  *
 *                                                                            *
 * Purpose: the function is called on agent startup                           *
 *          It should be used to call any initialization routines             *
 *                                                                            *
 * Return value: ZBX_MODULE_OK - success                                      *
 *               ZBX_MODULE_FAIL - module initialization failed               *
 *                                                                            *
 * Comment: the module won't be loaded in case of ZBX_MODULE_FAIL             *
 *                                                                            *
 ******************************************************************************/
int	zbx_module_init(void)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);
    zabbix_log(LOG_LEVEL_ERR, "Init %s ", PACKAGE_STRING );

    /* initialization for dummy.random */
    srand(time(NULL));
    executions=malloc(sizeof(execution_t *));
    *executions=NULL;

    L = luaL_newstate();
    luaL_openlibs(L);
    luaopen_base(L);             /* opens the basic library */
    luaopen_table(L);            /* opens the table library */
    luaopen_io(L);               /* opens the I/O library */
    luaopen_string(L);           /* opens the string lib. */
    luaopen_math(L);             /* opens the math lib. */

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return ZBX_MODULE_OK;
}

/******************************************************************************
 *                                                                            *
 * Function: zbx_module_uninit                                                *
 *                                                                            *
 * Purpose: the function is called on agent shutdown                          *
 *          It should be used to cleanup used resources if there are any      *
 *                                                                            *
 * Return value: ZBX_MODULE_OK - success                                      *
 *               ZBX_MODULE_FAIL - function failed                            *
 *                                                                            *
 ******************************************************************************/
int	zbx_module_uninit(void)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    if(*executions != NULL) {
        webmon_free_execution(*executions);
    }

    if(L != NULL) {
        lua_close(L);
    }

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return ZBX_MODULE_OK;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_replace_vars                                              *
 *                                                                            *
 * Purpose: Remplace vars by value in string                                  *
 *                                                                            *
 * Return value: value with vars remplacing                                   *
 *                                                                            *
 ******************************************************************************/
char *webmon_replace_vars(step_t *var_repo, char *value_template)
{
    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return value_template;
}

/******************************************************************************
 *                                                                            *
 * Function: webmon_exec_lua                                          *
 *                                                                            *
 * Purpose: Exec lua code                                             *
 *                                                                            *
 * Return value: size of current buffer                                       *
 *                                                                            *
 ******************************************************************************/
 char *webmon_exec_lua(char *code, const char *url, const char *body)
 {
    const char *code_header="function calculate(url, body)";
    const char *code_footer="end";
    int size = strlen(code) + strlen(code_header) + strlen(code_footer) +4;

    zabbix_log(LOG_LEVEL_DEBUG, "In %s()", __func__);

    char *formated_code= (char *) zbx_malloc(NULL, (size)*sizeof(char));
    zbx_snprintf(formated_code, size, "%s\n%s\n%s", code_header, code, code_footer);
    zabbix_log(LOG_LEVEL_WARNING, "Exec lua code : %s", formated_code );
    if (luaL_loadbuffer(L, formated_code, strlen(formated_code), "line") != LUA_OK) {
        zabbix_log(LOG_LEVEL_ERR, "LUA ERRROR: %s", lua_tostring(L, -1));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        zbx_free(formated_code);
        return strdup("");
    }
    if (lua_pcall(L, 0, 1, 0) != LUA_OK) {
        zabbix_log(LOG_LEVEL_ERR, "LUA ERRROR: %s", lua_tostring(L, -1));
        zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
        zbx_free(formated_code);
        return strdup("");
    }

    // Put the function to be called onto the stack
    lua_getglobal(L, "calculate");
    //lua_pushinteger(L, 3);  // first argument
    lua_pushstring(L, url);
    lua_pushstring(L, body);
    zbx_free(formated_code);
    int error = lua_pcall(L, 2, 1, 0);
    if (error) {
        zabbix_log(LOG_LEVEL_ERR, "LUA ERRROR: %s", lua_tostring(L, -1));
        lua_pop(L, 1);  /* pop error message from the stack */
        return strdup("");
    } else {
        if(lua_isstring(L, -1)) {
            char *val=lua_tolstring(L, -1, NULL);
            char *rval=strdup(val);
            zabbix_log(LOG_LEVEL_WARNING, "LUA RETURN: val=%s", val);

            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return rval;
        } else {
            zabbix_log(LOG_LEVEL_ERR, "LUA ERRROR: return value is not a string %s", lua_tostring(L, -1));
            zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
            return strdup("");
        }
    }
	zabbix_log(LOG_LEVEL_DEBUG, "End of %s()", __func__);
    return strdup("OK");
 }
